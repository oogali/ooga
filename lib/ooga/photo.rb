require 'rubygems'
require 'redis/connection/hiredis'
require 'redis'
require 'redis/objects'
require 'redis/value'
require 'addressable/uri'
require 'exifr'
require 'open-uri'
require 'time'

class Photo
  include Redis::Objects

  attr_accessor :id
  counter :views
  value :title
  value :description
  value :uri
  value :captured
  value :overlay
  hash_key :location
  hash_key :dimensions

  def initialize(gallery = 'default', prefix = 'ooga::llery/photos')
    Photo.redis_prefix = "#{prefix}/#{gallery}"
  end

  def Photo.find(photo_id, gallery = 'default', prefix = 'ooga::llery/photos')
    photo_id = photo_id.to_i
    return nil if photo_id <= 0

    photo = Photo.new gallery, prefix
    photo.id = photo_id

    photo = nil if photo.uri.to_s.empty?

    photo
  end
  
  def set_uri(uri)
    uri = Addressable::URI.parse uri
    return nil if !uri
    
    exif = EXIFR::JPEG.new open(uri)
    self.dimensions[:width] = exif.width
    self.dimensions[:height] = exif.height
    self.captured = exif.date_time_original
    self.uri = uri
  end
  
  def to_h
    {
      :id => self.id,
      :views => self.views.to_i,
      :title => self.title.to_s,
      :description => self.description.to_s,
      :uri => self.uri.to_s,
      :captured => Time.parse(self.captured.to_s),
      :overlay => self.overlay.to_s,
      :location => {
        :name => self.location[:name],
        :city => self.location[:city],
        :state => self.location[:state]
      },
      :dimensions => {
        :width => self.dimensions[:width],
        :height => self.dimensions[:height]
      }
    }
  end
end