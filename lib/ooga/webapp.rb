require 'rubygems'
require 'sinatra'
require 'sinatra/async'
require 'sinatra/settings'
require 'haml'
require 'rack-flash'
require 'ooga'
require 'time'
require 'redis/counter'
require 'bcrypt'
require 'json'

module OoGa
  class Application < Sinatra::Application
    register Sinatra::Async
    configure :development do
      register Sinatra::Settings
      enable :show_settings
    end
    use Rack::Session::Pool
    use Rack::Flash

    helpers do
      def authenticate(username, password)
        auth = Redis::HashKey.new("ooga::llery/auth")
        return false if not username or not auth[username]

        session[:username] = BCrypt::Password.new(auth[username].to_s) == password ? username : nil
        session[:username] and not session[:username].empty?
      end
    end

    before do
      if request.path_info.match /^\/new/ and (!session[:username] or session[:username].empty?)
        redirect '/photo/login'
      end
    end

    after do
      case request.path_info
        when /^\/js\//
          content_type 'text/javascript', :charset => 'utf-8'
          expires 86400, :public, :must_revalidate
        when /^\/css\//
          content_type 'text/css', :charset => 'utf-8'
          expires 86400, :public, :must_revalidate
        when /^\/login/
          expires 0, :private, :no_cache
        else
          expires 60, :public, :must_revalidate
      end
    end

    aget '/' do
      redirect '/photo/1'
    end

    aget '/healthcheck' do 
      body { 'OK' }
    end

    aget '/new' do
      body { haml :new }
    end

    aget '/login' do
      body { haml :login }
    end

    post '/login/verify' do
      if !params[:username] or params[:username].empty? or !params[:password] or params[:password].empty? or !authenticate(params[:username], params[:password])
        flash[:error] = 'Please enter a valid username and password.'
        return redirect '/photo/login'
      end

      redirect '/photo/new'
    end

    # important note: route order matters.
    
    get '/:id.json' do
      @image = Photo.find params[:id].to_i
      body { JSON.pretty_generate(@image.to_h) + "\n" }
    end

    get '/:id' do
      @image = Photo.find params[:id].to_i
      redirect '/photo/1' if not @image

      @title = "#{@image.id} | #{@image.title}"
      @tall = @image.dimensions[:height].to_i < 750
      width = @image.dimensions[:width].to_i
      if @image.overlay.to_s == 'right'
        @left = {
          :folder => (width > 1030 ? 1040 : width) - 125,
          :descr => (width > 1030 ? 1040 : width) - 300
        }
      else
        if width <= 1030
          @left = {
            :folder => 1030 - width - (95 + ((1030 - width) / 2)) + 5,
            :descr => 1030 - width - (5 + ((1030 - width) / 2)) + 5
          }
        else
          @left = {
            :folder => -105,
            :descr => -15
          }
        end
      end

      body {
        if params[:fragment]
          @fragment = true
          haml :photo, :layout => false
        elsif params[:css]
          haml :photo_css, :layout => false
        else
          haml :photo
        end
      }
    end
    
    post '/edit' do
      @photo = Photo.find(params[:id].to_i) || Photo.new
      @photo.id ||= Redis::Counter.new('ooga::llery/id_seq').increment
      @photo.set_uri params[:uri]
      @photo.title = params[:title]
      @photo.overlay = params[:overlay]
      @photo.description = params[:description]
      @photo.location[:name] = params[:loc_name]
      @photo.location[:city] = params[:loc_city]
      @photo.location[:state] = params[:loc_state]
      @photo.captured = Time.parse params[:captured]

      redirect "/photo/#{@photo.id}"
    end

    aget %r{/css/(default|reset)\.css} do |css|
      content_type 'text/css', :charset => 'utf-8'
      body { sass :"#{css}" }
    end
  end
end
