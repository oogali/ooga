ooga::llery
===========

This is my quick-and-dirty attempt at building a really, simple gallery app, backed by Redis.

ooga makes heavy use of [redis-objects](https://github.com/nateware/redis-objects) by Nate Wiger, and also [exifr](https://github.com/remvee/exifr) by Remco van 't Veer.


What do I need?
---------------

* Ruby
* [Bundler](http://gembundler.com) -- `gem install bundler`
* [Redis](http://redis.io)


Contributing
------------

* Fork this Git repository
* Create a new branch
* Commit and push
* Submit a pull request


Installation
------------

    git clone git://github.com/oogali/ooga.git
    cd ooga
    bundle install

Optionally, edit your Redis server parameters in `setup/redis.rb`.


Usage
-----

    bundle exec rake start
    open http://localhost:18205/photo/new


Demo
----
I'm running it for my test gallery, at [omachonuogali.com/photo](http://omachonuogali.com/photo/).